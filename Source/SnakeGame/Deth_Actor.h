// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Interactable.h"
#include "Deth_Actor.generated.h"

class USceneComponent;
class ASnakeBase;
class ASnakeElementBase;
class AFood;

UCLASS()
class SNAKEGAME_API ADeth_Actor : public AActor, public IInteractable
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ADeth_Actor();

	UPROPERTY(BlueprintReadWrite)
		ADeth_Actor* DethWall;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;


	UPROPERTY(EditAnywhere)
		class USceneComponent* DethComponent;

	UPROPERTY(EditAnywhere)
		UMaterialInstance* DethColor;

	//UFUNCTION()
	//void CollidWallDeth();

	virtual void IntDethAct(AActor* IntDethActor, bool bIsDeth) override;

	virtual void Interact(AActor* Interactor, bool bIsHead) override;

};
