// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Pawn.h"
#include "PlayerPawnBase.generated.h"

class UCameraComponent;
class ASnakeBase;
class ASnakeElementBase;

UCLASS()
class SNAKEGAME_API APlayerPawnBase : public APawn
{
	GENERATED_BODY()

public:
	// Sets default values for this pawn's properties
	APlayerPawnBase();

	UPROPERTY(BlueprintReadWrite)
		UCameraComponent* PawnCamera;

	UPROPERTY(BlueprintReadWrite)
		ASnakeBase* SnakeActor;

	UPROPERTY(EditDefaultsOnly)
		TSubclassOf<ASnakeBase> SnakeActorClass;

	//UPROPERTY(EditDefaultsOnly)
		int32 GameMode = 0;

		float MinY = -970.f;
		float MaxY = 970.f;
		float MinX = -420.f;
		float MaxX = 420.f;
		float SpawnZ = 50.f;

		float StepDeley = 2.f;
		float BuferTime = 0;

	UPROPERTY(EditDefaultsOnly)
		bool ButtonClick = true;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UFUNCTION(BlueprintCallable, Category = "PlayerPawn")
		void CreateSnakeActor();

	UFUNCTION(BlueprintCallable, Category = "PlayerPawn")
		int32 GetGameMode() const { return GameMode; }

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	//UFUNCTION(BlueprintCallable, Category = "PlayerPawn")
		//void CreateSnakeActor();

	//UFUNCTION(BlueprintCallable, Category = "PlayerPawn")
		//int32 GetGameMode() const { return GameMode; }

	void AddRandomFood();

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	UFUNCTION()
		void HandlePlayerVerticalInput(float value);
	UFUNCTION()
		void HandlePlayerHorizontalInput(float value);

	//float MinY = -970.f; 
	//float MaxY = 970.f; 
	//float MinX = -420.f; 
	//float MaxX = 420.f;
	//float SpawnZ = 50.f;

	//float StepDeley = 2.f;
	//float BuferTime = 0;

};
