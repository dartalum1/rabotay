// Fill out your copyright notice in the Description page of Project Settings.


#include "Deth_Actor.h"
#include "SnakeBase.h"
#include "Food.h"
#include "PlayerPawnBase.h"
#include "Interactable.h"
#include "SnakeElementBase.h"

// Sets default values
ADeth_Actor::ADeth_Actor()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	class UStaticMesh* DethMesh = ConstructorHelpers::FObjectFinder<UStaticMesh>(TEXT("/Engine/BasicShapes/Cube")).Object;

	DethColor = ConstructorHelpers::FObjectFinderOptional<UMaterialInstance>(TEXT
    ("MaterialInstanceConstant'/Engine/VREditor/LaserPointer/TranslucentLaserPointerMaterialInst.TranslucentLaserPointerMaterialInst'")).Get();

	DethComponent = CreateAbstractDefaultSubobject<USceneComponent>("RootDeth");
	RootComponent = DethComponent;

	class UStaticMeshComponent* WallDeth;
	WallDeth = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("WALL"));
	WallDeth->SetStaticMesh(DethMesh);
	WallDeth->SetRelativeLocation(FVector(0, 0, 0));
	WallDeth->SetMaterial(0, DethColor);
	WallDeth->AttachTo(DethComponent);
}

// Called when the game starts or when spawned
void ADeth_Actor::BeginPlay()
{
	Super::BeginPlay();

}
// Called every frame
void ADeth_Actor::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	//CollidWallDeth();
}

void ADeth_Actor::IntDethAct(AActor* IntDethActor, bool bIsDeth)
{
	if (bIsDeth)
	{
		auto SnakeFood = Cast<AFood>(IntDethActor);
		if (IsValid(SnakeFood))
		{
			SnakeFood->Destroy(true, true);
		}
	}
}

void ADeth_Actor::Interact(AActor* Interactor, bool bIsHead)
{
	if (bIsHead)
	{
		auto Snake = Cast<ASnakeBase>(Interactor);
		if (IsValid(Snake))
		{
			Snake->Destroy(true, true);
		}
		
	}
}

//void ADeth_Actor::CollidWallDeth()
//{
	//TArray<AActor*> CollectedActors;
	//GetOverlappingActors(CollectedActors);

	//for (int32 i = 0; i < CollectedActors.Num(); ++i)
	//{
		//CollectedActors[i]->Destroy(true, true);
	//}
//}





