// Fill out your copyright notice in the Description page of Project Settings.


#include "Food.h"
#include "SnakeBase.h"
#include "Deth_Actor.h"
#include "Interactable.h"
#include "PlayerPawnBase.h"
#include "SnakeElementBase.h"


// Sets default values
AFood::AFood()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;


	FoodMeshs = ConstructorHelpers::FObjectFinder<UStaticMesh>(TEXT("/Engine/BasicShapes/Sphere")).Object;

	FoodColor = ConstructorHelpers::FObjectFinderOptional<UMaterialInstance>(TEXT
	("MaterialInstanceConstant'/Game/Material_SG/LaserPointerMaterialInst.LaserPointerMaterialInst'")).Get();

	FoodComponent = CreateDefaultSubobject<USceneComponent>("RootFood");
	RootComponent = FoodComponent;

	class UStaticMeshComponent* FoodYummy;
	FoodYummy = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Food"));
	FoodYummy->SetStaticMesh(FoodMeshs);
	FoodYummy->SetRelativeLocation(FVector(0, 0, 0));
	FoodYummy->SetMaterial(0, FoodColor);
	FoodYummy->AttachTo(FoodComponent);
	FoodYummy->SetSimulatePhysics(true);

}
	

// Called when the game starts or when spawned
void AFood::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AFood::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	
}

 void AFood::Interact(AActor* Interactor, bool bIsHead)
{
	 if (bIsHead)
	 {
		 auto Snake = Cast<ASnakeBase>(Interactor);
		 if (IsValid(Snake))
		 {
			 
			 Snake->AddSnakeElement();
			 Destroy();
			
			 
		 }
	 }
	 
}

