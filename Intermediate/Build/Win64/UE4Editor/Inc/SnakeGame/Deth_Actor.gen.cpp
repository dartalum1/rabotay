// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "SnakeGame/Deth_Actor.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeDeth_Actor() {}
// Cross Module References
	SNAKEGAME_API UClass* Z_Construct_UClass_ADeth_Actor_NoRegister();
	SNAKEGAME_API UClass* Z_Construct_UClass_ADeth_Actor();
	ENGINE_API UClass* Z_Construct_UClass_AActor();
	UPackage* Z_Construct_UPackage__Script_SnakeGame();
	ENGINE_API UClass* Z_Construct_UClass_USceneComponent_NoRegister();
	ENGINE_API UClass* Z_Construct_UClass_UMaterialInstance_NoRegister();
	SNAKEGAME_API UClass* Z_Construct_UClass_UInteractable_NoRegister();
// End Cross Module References
	void ADeth_Actor::StaticRegisterNativesADeth_Actor()
	{
	}
	UClass* Z_Construct_UClass_ADeth_Actor_NoRegister()
	{
		return ADeth_Actor::StaticClass();
	}
	struct Z_Construct_UClass_ADeth_Actor_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DethWall_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_DethWall;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DethComponent_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_DethComponent;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DethColor_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_DethColor;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FImplementedInterfaceParams InterfaceParams[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_ADeth_Actor_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_AActor,
		(UObject* (*)())Z_Construct_UPackage__Script_SnakeGame,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ADeth_Actor_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "Deth_Actor.h" },
		{ "ModuleRelativePath", "Deth_Actor.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ADeth_Actor_Statics::NewProp_DethWall_MetaData[] = {
		{ "Category", "Deth_Actor" },
		{ "ModuleRelativePath", "Deth_Actor.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_ADeth_Actor_Statics::NewProp_DethWall = { "DethWall", nullptr, (EPropertyFlags)0x0010000000000004, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ADeth_Actor, DethWall), Z_Construct_UClass_ADeth_Actor_NoRegister, METADATA_PARAMS(Z_Construct_UClass_ADeth_Actor_Statics::NewProp_DethWall_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ADeth_Actor_Statics::NewProp_DethWall_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ADeth_Actor_Statics::NewProp_DethComponent_MetaData[] = {
		{ "Category", "Deth_Actor" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Deth_Actor.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_ADeth_Actor_Statics::NewProp_DethComponent = { "DethComponent", nullptr, (EPropertyFlags)0x0010000000080009, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ADeth_Actor, DethComponent), Z_Construct_UClass_USceneComponent_NoRegister, METADATA_PARAMS(Z_Construct_UClass_ADeth_Actor_Statics::NewProp_DethComponent_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ADeth_Actor_Statics::NewProp_DethComponent_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ADeth_Actor_Statics::NewProp_DethColor_MetaData[] = {
		{ "Category", "Deth_Actor" },
		{ "ModuleRelativePath", "Deth_Actor.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_ADeth_Actor_Statics::NewProp_DethColor = { "DethColor", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ADeth_Actor, DethColor), Z_Construct_UClass_UMaterialInstance_NoRegister, METADATA_PARAMS(Z_Construct_UClass_ADeth_Actor_Statics::NewProp_DethColor_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ADeth_Actor_Statics::NewProp_DethColor_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_ADeth_Actor_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ADeth_Actor_Statics::NewProp_DethWall,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ADeth_Actor_Statics::NewProp_DethComponent,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ADeth_Actor_Statics::NewProp_DethColor,
	};
		const UE4CodeGen_Private::FImplementedInterfaceParams Z_Construct_UClass_ADeth_Actor_Statics::InterfaceParams[] = {
			{ Z_Construct_UClass_UInteractable_NoRegister, (int32)VTABLE_OFFSET(ADeth_Actor, IInteractable), false },
		};
	const FCppClassTypeInfoStatic Z_Construct_UClass_ADeth_Actor_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<ADeth_Actor>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_ADeth_Actor_Statics::ClassParams = {
		&ADeth_Actor::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_ADeth_Actor_Statics::PropPointers,
		InterfaceParams,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_ADeth_Actor_Statics::PropPointers),
		UE_ARRAY_COUNT(InterfaceParams),
		0x009000A4u,
		METADATA_PARAMS(Z_Construct_UClass_ADeth_Actor_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_ADeth_Actor_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_ADeth_Actor()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_ADeth_Actor_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(ADeth_Actor, 500998257);
	template<> SNAKEGAME_API UClass* StaticClass<ADeth_Actor>()
	{
		return ADeth_Actor::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_ADeth_Actor(Z_Construct_UClass_ADeth_Actor, &ADeth_Actor::StaticClass, TEXT("/Script/SnakeGame"), TEXT("ADeth_Actor"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(ADeth_Actor);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
